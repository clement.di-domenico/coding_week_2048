import random


def create_grid(size):
    game_grid = []
    for i in range(0, size):
        game_grid.append([' '] * size)
    return game_grid


def grid_add_new_tile_at_position(grid, position_x, position_y):
    grid[position_x][position_y] = get_value_new_tile()
    return grid


def get_all_tiles(grid):
    result = []
    for line in grid:
        for tile in line:
            result.append(0 if tile == ' ' else tile)
    return result


def get_value_new_tile():
    return 2 if random.random() < 0.9 else 4


def get_empty_tiles_positions(grid):
    result = []
    for index_x in range(len(grid)):
        for index_y in range(len(grid[0])):
            if grid[index_x][index_y] in [0, ' ']:
                result.append((index_x, index_y))
    return result


def grid_get_value(grid, position_x, position_y):
    return grid[position_x][position_y]


def get_new_position(grid):
    empty_tiles = get_empty_tiles_positions(grid)
    position = empty_tiles[random.randint(0, len(empty_tiles) - 1)]
    grid[position[0]][position[1]] = 0
    return position
